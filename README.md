# Flectra Community / reporting-engine

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[report_qr](report_qr/) | 2.0.1.0.1| Web QR Manager
[report_qweb_encrypt](report_qweb_encrypt/) | 2.0.1.0.1| Allow to encrypt qweb pdfs
[report_qweb_signer](report_qweb_signer/) | 2.0.1.0.1| Sign Qweb PDFs usign a PKCS#12 certificate
[base_comment_template](base_comment_template/) | 2.0.3.0.1| Add conditional mako template to any reporton models that inherits comment.template.
[report_qweb_element_page_visibility](report_qweb_element_page_visibility/) | 2.0.1.0.0| Report Qweb Element Page Visibility
[bi_view_editor](bi_view_editor/) | 2.0.1.0.0| Graphical BI views builder for Odoo
[report_async](report_async/) | 2.0.1.0.0| Central place to run reports live or async
[report_xlsx](report_xlsx/) | 2.0.1.0.8| Base module to create xlsx report
[kpi_dashboard_bokeh](kpi_dashboard_bokeh/) | 2.0.1.0.0|         Create dashboards using bokeh
[bi_sql_editor](bi_sql_editor/) | 2.0.1.0.1| BI Views builder, based on Materialized or Normal SQL Views
[report_qweb_parameter](report_qweb_parameter/) | 2.0.1.0.0|         Add new parameters for qweb templates in order to reduce field length        and check minimal length    
[report_wkhtmltopdf_param](report_wkhtmltopdf_param/) | 2.0.1.0.0|         Add new parameters for a paper format to be used by wkhtmltopdf        command as arguments.    
[report_xlsx_helper](report_xlsx_helper/) | 2.0.1.0.2| Report xlsx helpers
[report_csv](report_csv/) | 2.0.1.0.0| Base module to create csv report
[report_xlsx_helper_demo](report_xlsx_helper_demo/) | 2.0.1.0.0| Report xlsx helpers - demo
[report_py3o](report_py3o/) | 2.0.1.0.2| Reporting engine based on Libreoffice (ODT -> ODT, ODT -> PDF, ODT -> DOC, ODT -> DOCX, ODS -> ODS, etc.)
[report_xml](report_xml/) | 2.0.1.0.3| Allow to generate XML reports
[report_py3o_fusion_server](report_py3o_fusion_server/) | 2.0.1.0.0| Let the fusion server handle format conversion.
[kpi_dashboard](kpi_dashboard/) | 2.0.1.1.0|         Create Dashboards using kpis
[report_qweb_pdf_watermark](report_qweb_pdf_watermark/) | 2.0.1.0.1| Add watermarks to your QWEB PDF reports
[report_layout_config](report_layout_config/) | 2.0.1.0.0| Add possibility to easily modify the global report layout


