# Copyright 2009-2019 Noviat.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Report xlsx helpers",
    "author": "Noviat, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/reporting-engine",
    "category": "Reporting",
    "version": "2.0.1.0.2",
    "license": "AGPL-3",
    "depends": ["report_xlsx"],
    "development_status": "Mature",
    "installable": True,
}
