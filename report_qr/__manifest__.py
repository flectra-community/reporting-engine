# Copyright 2019 Creu Blanca
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "Web QR Manager",
    "version": "2.0.1.0.1",
    "author": "Creu Blanca, " "Odoo Community Association (OCA)",
    "category": "Reporting",
    "website": "https://gitlab.com/flectra-community/reporting-engine",
    "license": "AGPL-3",
    "depends": ["web"],
    "data": [],
    "installable": True,
}
